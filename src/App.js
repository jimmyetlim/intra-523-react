import React, { Component } from 'react';

import axios from 'axios';
import './App.css';
import Message from './Message.js';
import TrouveNom from './TrouveNom.js';

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
      message: "Chercher un nom",
      nom: "",
      email: "",
      id: "",
    }
  }

  componentDidMount(username){
	const toSend = {"nom": username}
	axios({
		method:'post',
		url:'http://localhost:8081/getValue',
		responseType:'json',
		headers: {'Access-Control-Allow-Origin': "true"},
		data: toSend
		})
			  
		.then((response)=>{
			//this.setState({debug:response});
			this.setState({"nom":response.nom});
			this.setState({"email":response.email});
			this.setState({"id":response.id});
			this.setState({message: "Nom Trouver !!!"});
		
	});
      }

  render() {

    return (
      <div >
        <Message message={this.state.message} message={this.state.message}></Message>
        <TrouveNom trouveNom={this.trouveNom.bind(this)} send={this.trouveNom.bind(this)}/>
        <p>Nom: {this.state.nom}</p>
        <p>Email: {this.state.email}</p>
        <p>Id: {this.state.id}</p>
      </div>
    );
  }

  trouveNom(nom) {
    this.componentDidMount(nom);
  }
}

export default App;
