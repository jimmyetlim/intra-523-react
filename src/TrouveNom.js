import React, { Component } from 'react';

export default class TrouveNom extends Component {
constructor(props) {
    super(props)

    this.state = {
  
    }
  }
  render() {
    return (
      <div> 
	<form onSubmit={this.trouveNom.bind(this)}>
		<input type="text" ref="nameSearch" required autoFocus/>
		<button>Trouver</button> 	
	</form>
	</div>
    );
  }

  trouveNom(event) {
    this.props.send(this.refs.nameSearch);
  }

}
